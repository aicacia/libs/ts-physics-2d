import { vec2, vec4 } from "gl-matrix";
import {
  Camera2D,
  Camera2DControl,
  Entity,
  FullScreenCanvas,
  Input,
  Loop,
  Scene,
  Time,
  Transform2D,
} from "@aicacia/engine";
import {
  WebCanvas,
  WebEventListener,
  CtxRenderer,
  TransformCtxRendererHandler,
} from "@aicacia/engine/lib/web";
import {
  Body,
  BodyEvent,
  BodyType,
  Circle,
  RigidBody2D,
  World,
  World2D,
  GravityUniversal,
} from "../../src";
import { CtxBody2DRendererHandler } from "../../src/web";

const canvas = new WebCanvas().set(256, 256),
  body = new Body<Entity>()
    .setType(BodyType.Kinematic)
    .addShape(new Circle<Entity>().setDensity(200)),
  scene = new Scene()
    .addEntity(
      // Camera setup
      new Entity()
        .addTag("camera")
        .addComponent(
          new Transform2D().setRenderable(false),
          new Camera2DControl(),
          new Camera2D().setBackground(vec4.fromValues(0.98, 0.98, 0.98, 1.0))
        ),
      new Entity().addComponent(
        new Transform2D().setLocalPosition(vec2.fromValues(0, 5)),
        new RigidBody2D(new Body<Entity>().addShape(new Circle()))
          .on(BodyEvent.COLLIDING, () => {
            console.log("colliding");
          })
          .on(BodyEvent.COLLIDE_START, () => {
            console.log("collide-start");
          })
          .on(BodyEvent.COLLIDE_END, () => {
            console.log("collide-end");
          })
      ),
      new Entity().addComponent(new Transform2D(), new RigidBody2D(body))
    )
    .addPlugin(
      // Required by many Components and plugins
      new Time(),
      // Handles all input
      new Input().addEventListener(new WebEventListener(canvas.getElement())),
      // forces a canvas to stay in sync with the window size
      new FullScreenCanvas(canvas),
      new CtxRenderer(
        canvas,
        canvas.getElement().getContext("2d")
      ).addRendererHandler(
        new TransformCtxRendererHandler(),
        new CtxBody2DRendererHandler()
      ),
      new World2D(new World<Entity>().addConstraint(new GravityUniversal()))
    ),
  loop = new Loop(() => scene.update());

(window as any).scene = scene;
(window as any).loop = loop;

window.addEventListener("load", () => {
  document.body.appendChild(canvas.getElement());
  loop.start();
});
