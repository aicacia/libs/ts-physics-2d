export declare enum BodyType {
    Static = 0,
    Dynamic = 1,
    Kinematic = 2
}
