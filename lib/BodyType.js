"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BodyType = void 0;
var BodyType;
(function (BodyType) {
    BodyType[BodyType["Static"] = 0] = "Static";
    BodyType[BodyType["Dynamic"] = 1] = "Dynamic";
    BodyType[BodyType["Kinematic"] = 2] = "Kinematic";
})(BodyType = exports.BodyType || (exports.BodyType = {}));
