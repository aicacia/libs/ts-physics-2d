import { Component, Entity } from "@aicacia/engine";
import { Body } from "../Body";
import { BodyEvent } from "../BodyEvent";
import { Contact } from "../phases/Contact";
import { World2D } from "../plugins/World2D";
import { RigidBody2DManager } from "./RigidBody2DManager";
export interface RigidBody2D {
    on(event: "add-to-scene" | "remove-from-scene", listener: () => void): this;
    on(event: BodyEvent.COLLIDING | BodyEvent.COLLIDE_START | BodyEvent.COLLIDE_END, listener: (this: RigidBody2D, otherBody: RigidBody2D, contact: Contact<Entity>) => void): this;
}
export declare class RigidBody2D extends Component {
    static Manager: typeof RigidBody2DManager;
    static requiredPlugins: (typeof World2D)[];
    private body;
    constructor(body: Body<Entity>);
    getBody(): Body<Entity>;
    onAdd(): this;
    onRemove(): this;
    onUpdate(): this;
    private onColliding;
    private onCollideStart;
    private onCollideEnd;
}
