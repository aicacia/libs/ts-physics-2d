"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RigidBody2DManager = void 0;
var tslib_1 = require("tslib");
var engine_1 = require("@aicacia/engine");
var RigidBody2DManager = /** @class */ (function (_super) {
    tslib_1.__extends(RigidBody2DManager, _super);
    function RigidBody2DManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return RigidBody2DManager;
}(engine_1.DefaultManager));
exports.RigidBody2DManager = RigidBody2DManager;
