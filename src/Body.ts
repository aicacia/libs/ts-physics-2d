import { none, Option } from "@aicacia/core";
import { EventEmitter } from "events";
import { mat2d, vec2 } from "gl-matrix";
import { composeMat2d } from "@aicacia/engine";
import { BodyEvent } from "./BodyEvent";
import { BodyType } from "./BodyType";
import { Shape } from "./shapes";

const VEC2_0 = vec2.create(),
  VEC2_1 = vec2.create(),
  VEC2_SCALE_0 = vec2.fromValues(1, 1);

// tslint:disable-next-line: interface-name
export interface Body<UserData> {
  on(
    event:
      | BodyEvent.COLLIDING
      | BodyEvent.COLLIDE_START
      | BodyEvent.COLLIDE_END,
    listener: (
      this: Body<UserData>,
      otherBody: Body<UserData>,
      contact: Contact<UserData>
    ) => void
  ): this;
}

export class Body<UserData> extends EventEmitter {
  protected type: BodyType = BodyType.Dynamic;
  protected world: Option<World<UserData>> = none();
  protected userData: Option<UserData> = none();

  protected velocity: vec2 = vec2.create();
  protected angularVelocity = 0;

  protected linearDamping: Option<number> = none();
  protected angularDamping: Option<number> = none();

  protected mass = 1.0;
  protected invMass = 1.0;

  protected inertia = 0.0;
  protected invInertia = 0.0;

  protected aabb: AABB2 = AABB2.create();

  protected position: vec2 = vec2.create();
  protected rotation = 0;

  protected matrix: mat2d = mat2d.create();

  protected needsUpdate = false;
  protected aabbNeedsUpdate = false;

  protected shapes: Array<Shape<UserData>> = [];

  getType() {
    return this.type;
  }

  getUserData() {
    return this.userData;
  }
  setUserData(userData: UserData) {
    this.userData.replace(userData);
    return this;
  }

  UNSAFE_setWorld(world: World<UserData>) {
    this.world.replace(world);
    return this;
  }
  UNSAFE_removeWorld() {
    this.world.take();
    return this;
  }
  getWorld() {
    return this.world;
  }

  getAABB() {
    return this.updateAABBIfNeeded().aabb;
  }
  getShapes(): ReadonlyArray<Shape<UserData>> {
    return this.shapes;
  }

  getPosition() {
    return this.position;
  }
  setPosition(position: vec2) {
    vec2.copy(this.position, position);
    return this.setNeedsUpdate();
  }

  getRotation() {
    return this.rotation;
  }
  setRotation(rotation: number) {
    this.rotation = rotation;
    return this.setNeedsUpdate();
  }

  setNeedsUpdate(needsUpdate = true) {
    if (needsUpdate !== this.needsUpdate) {
      this.needsUpdate = needsUpdate;
      this.setAABBNeedsUpdate(needsUpdate);
    }
    return this;
  }
  getNeedsUpdate() {
    return this.needsUpdate;
  }

  setAABBNeedsUpdate(aabbNeedsUpdate = true) {
    if (aabbNeedsUpdate !== this.aabbNeedsUpdate) {
      this.aabbNeedsUpdate = aabbNeedsUpdate;
      this.shapes.forEach((shape) => shape.setNeedsUpdate(aabbNeedsUpdate));
    }
    return this;
  }
  getAABBNeedsUpdate() {
    return this.aabbNeedsUpdate;
  }

  getMatrix() {
    return this.updateMatrixIfNeeded().matrix;
  }

  updateMatrixIfNeeded() {
    if (this.getNeedsUpdate()) {
      return this.updateMatrix();
    } else {
      return this;
    }
  }
  updateMatrix() {
    this.needsUpdate = false;
    composeMat2d(this.matrix, this.position, VEC2_SCALE_0, this.rotation);
    return this;
  }

  updateAABBIfNeeded() {
    if (this.getAABBNeedsUpdate()) {
      return this.updateAABB();
    } else {
      return this;
    }
  }
  updateAABB() {
    this.aabbNeedsUpdate = false;
    this.shapes.reduce((aabb, shape) => {
      AABB2.union(aabb, aabb, shape.getAABB());
      return aabb;
    }, AABB2.identity(this.aabb));
    return this;
  }

  addShapes(shapes: Array<Shape<UserData>>) {
    shapes.forEach((shape) => this._addShape(shape));
    return this.resetMassData();
  }
  addShape(...shapes: Array<Shape<UserData>>) {
    return this.addShapes(shapes);
  }

  setType(type: BodyType) {
    if (type !== this.type) {
      this.type = type;
      return this.resetMassData();
    } else {
      return this;
    }
  }

  getLinearDamping(): number {
    return this.linearDamping.unwrapOrElse(() =>
      this.world
        .map((world) => world.getLinearDamping())
        .unwrapOr(DEFAULT_LINEAR_DAMPING)
    );
  }
  setLinearDamping(linearDamping: number) {
    this.linearDamping.replace(linearDamping);
    return this;
  }

  getAngularDamping() {
    return this.angularDamping.unwrapOrElse(() =>
      this.world
        .map((world) => world.getAngularDamping())
        .unwrapOr(DEFAULT_ANGULAR_DAMPING)
    );
  }
  setAngularDamping(angularDamping: number) {
    this.angularDamping.replace(angularDamping);
    return this;
  }

  getVelocity() {
    return this.velocity;
  }
  setVelocity(velocity: vec2) {
    vec2.copy(this.velocity, velocity);
    return this;
  }
  addVelocity(velocity: vec2) {
    vec2.add(this.velocity, this.velocity, velocity);
    return this;
  }

  getAngularVelocity() {
    return this.angularVelocity;
  }
  setAngularVelocity(angularVelocity: number) {
    this.angularVelocity = angularVelocity;
    return this;
  }
  addAngularVelocity(angularVelocity: number) {
    this.angularVelocity += angularVelocity;
    return this;
  }

  update(delta: number) {
    switch (this.type) {
      case BodyType.Static: {
        break;
      }
      default: {
        vec2.add(
          this.position,
          this.position,
          vec2.scale(VEC2_0, this.velocity, delta)
        );
        this.rotation += this.angularVelocity * delta;
        vec2.scale(this.velocity, this.velocity, 1.0 - this.getLinearDamping());
        this.angularVelocity =
          this.angularVelocity * (1.0 - this.getAngularDamping());
        this.setNeedsUpdate();
        break;
      }
    }
    return this;
  }

  getMass() {
    return this.mass;
  }
  getInvMass() {
    return this.invMass;
  }
  setMass(mass: number) {
    if (this.mass !== mass && mass > 0) {
      this.mass = mass;
      this.invMass = 1.0 / mass;
    }
    return this;
  }

  getInertia() {
    return this.inertia;
  }
  getInvInertia() {
    return this.invInertia;
  }
  setInertia(inertia: number) {
    if (this.inertia !== inertia && inertia > 0) {
      this.inertia = inertia;
      this.invInertia = 1.0 / inertia;
    }
    return this;
  }

  private resetMassData() {
    if (this.type !== BodyType.Static) {
      const totalCentroid = vec2.zero(VEC2_0);

      let totalMass = 0.0,
        totalInertia = 0.0;

      this.shapes.forEach((shape) => {
        const centroid = shape.getCentroid(VEC2_1),
          mass = shape.getArea() * shape.getDensity(),
          inertia = shape.getInertia(mass);

        vec2.add(
          totalCentroid,
          totalCentroid,
          vec2.scale(centroid, centroid, mass)
        );
        totalMass += mass;
        totalInertia += inertia;
      });

      this.setMass(totalMass);
      vec2.copy(
        totalCentroid,
        vec2.scale(totalCentroid, totalCentroid, this.invMass)
      );

      this.setMass(totalMass);
      this.setInertia(
        totalInertia - totalMass * vec2.squaredLength(totalCentroid)
      );
    }
    return this;
  }

  private _addShape<S extends Shape<UserData>>(shape: S) {
    shape.UNSAFE_setBody(this);
    this.shapes.push(shape);
    return this;
  }
}

import { Contact } from "./phases/Contact";
import {
  DEFAULT_ANGULAR_DAMPING,
  DEFAULT_LINEAR_DAMPING,
  World,
} from "./World";
import { AABB2 } from "./AABB2";
