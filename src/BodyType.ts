export enum BodyType {
  Static,
  Dynamic,
  Kinematic,
}
