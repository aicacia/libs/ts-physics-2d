import { none, Option } from "@aicacia/core";
import { EventEmitter } from "events";
import { World } from "./World";

export abstract class Constraint<UserData> extends EventEmitter {
  protected world: Option<World<UserData>> = none();

  UNSAFE_setWorld(world: World<UserData>) {
    this.world.replace(world);
    return this;
  }
  UNSAFE_removeWorld() {
    this.world.take();
    return this;
  }
  getWorld() {
    return this.world;
  }

  abstract update(delta: number): this;
}
