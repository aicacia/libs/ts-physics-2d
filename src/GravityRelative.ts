import { vec2 } from "gl-matrix";
import { Body } from "./Body";
import { BodyType } from "./BodyType";
import { Constraint } from "./Constraint";

const VEC2_0 = vec2.create(),
  VEC2_1 = vec2.create();

export class GravityRelative<UserData> extends Constraint<UserData> {
  static G = 6.6743e-11;

  protected constaint: number = GravityRelative.G;
  protected body: Body<UserData>;

  constructor(body: Body<UserData>) {
    super();
    this.body = body;
  }

  getConstaint() {
    return this.constaint;
  }
  setConstaint(constaint: number) {
    this.constaint = constaint;
    return this;
  }

  getBody() {
    return this.body;
  }
  setBody(body: Body<UserData>) {
    this.body = body;
    return this;
  }

  update(delta: number) {
    this.getWorld().ifSome((world) => {
      const massA = this.body.getMass(),
        positionA = this.body.getPosition();

      world.getBodies().forEach((bodyB) => {
        if (bodyB.getType() === BodyType.Dynamic) {
          const positionB = bodyB.getPosition(),
            direction = vec2.subtract(VEC2_0, positionA, positionB),
            rsq = vec2.squaredLength(direction);

          if (rsq > 0) {
            const bodyVelocity = bodyB.getVelocity(),
              invRsq = 1.0 / rsq,
              linearVelocity = this.constaint * massA * invRsq * delta,
              gravityVelocity = vec2.scale(
                VEC2_1,
                vec2.normalize(direction, direction),
                linearVelocity
              );

            vec2.add(bodyVelocity, bodyVelocity, gravityVelocity);
          }
        }
      });
    });
    return this;
  }
}
