import { vec2 } from "gl-matrix";
import { BodyType } from "./BodyType";
import { Constraint } from "./Constraint";

const VEC2_0 = vec2.create();

export class GravityUniversal<UserData> extends Constraint<UserData> {
  protected gravity: vec2 = vec2.fromValues(0, -9.801);

  update(delta: number) {
    this.getWorld().ifSome((world) => {
      const gravityVelocity = vec2.scale(VEC2_0, this.gravity, delta);

      world.getBodies().forEach((body) => {
        if (body.getType() === BodyType.Dynamic) {
          const velocity = body.getVelocity();
          vec2.add(velocity, velocity, gravityVelocity);
        }
      });
    });
    return this;
  }
}
