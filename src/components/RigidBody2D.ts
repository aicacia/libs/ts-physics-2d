import { Component, Entity, TransformComponent } from "@aicacia/engine";
import { Body } from "../Body";
import { BodyEvent } from "../BodyEvent";
import { BodyType } from "../BodyType";
import { Contact } from "../phases/Contact";
import { World2D } from "../plugins/World2D";
import { RigidBody2DManager } from "./RigidBody2DManager";

// tslint:disable-next-line: interface-name
export interface RigidBody2D {
  on(event: "add-to-scene" | "remove-from-scene", listener: () => void): this;
  on(
    event:
      | BodyEvent.COLLIDING
      | BodyEvent.COLLIDE_START
      | BodyEvent.COLLIDE_END,
    listener: (
      this: RigidBody2D,
      otherBody: RigidBody2D,
      contact: Contact<Entity>
    ) => void
  ): this;
}

export class RigidBody2D extends Component {
  static Manager = RigidBody2DManager;
  static requiredPlugins = [World2D];

  private body: Body<Entity>;

  constructor(body: Body<Entity>) {
    super();

    this.body = body;
  }

  getBody() {
    return this.body;
  }

  onAdd() {
    this.body.setUserData(this.getRequiredEntity());

    TransformComponent.getTransform(this.getRequiredEntity()).ifSome(
      (transform) => {
        transform.getLocalPosition2(this.body.getPosition());
        this.body.setRotation(transform.getLocalRotationZ());
      }
    );

    this.getRequiredPlugin(World2D).getWorld().addBody(this.body);

    this.body.on(BodyEvent.COLLIDING, this.onColliding);
    this.body.on(BodyEvent.COLLIDE_START, this.onCollideStart);
    this.body.on(BodyEvent.COLLIDE_END, this.onCollideEnd);

    return this;
  }

  onRemove() {
    this.body.off(BodyEvent.COLLIDING, this.onColliding);
    this.body.off(BodyEvent.COLLIDE_START, this.onCollideStart);
    this.body.off(BodyEvent.COLLIDE_END, this.onCollideEnd);

    this.getRequiredPlugin(World2D).getWorld().removeBody(this.body);
    return this;
  }

  onUpdate() {
    TransformComponent.getTransform(this.getRequiredEntity()).ifSome(
      (transform) => {
        switch (this.body.getType()) {
          case BodyType.Kinematic:
          case BodyType.Static:
            transform.getLocalPosition2(this.body.getPosition());
            this.body.setRotation(transform.getLocalRotationZ());
            break;
          default:
            transform.setLocalPosition2(this.body.getPosition());
            transform.setLocalRotationZ(this.body.getRotation());
            break;
        }
      }
    );
    return this;
  }

  private onColliding = (body: Body<Entity>, contact: Contact<Entity>) =>
    this.emit(
      BodyEvent.COLLIDING,
      body.getUserData().unwrap().getRequiredComponent(RigidBody2D),
      contact
    );
  private onCollideStart = (body: Body<Entity>, contact: Contact<Entity>) =>
    this.emit(
      BodyEvent.COLLIDE_START,
      body.getUserData().unwrap().getRequiredComponent(RigidBody2D),
      contact
    );
  private onCollideEnd = (body: Body<Entity>, contact: Contact<Entity>) =>
    this.emit(
      BodyEvent.COLLIDE_END,
      body.getUserData().unwrap().getRequiredComponent(RigidBody2D),
      contact
    );
}
