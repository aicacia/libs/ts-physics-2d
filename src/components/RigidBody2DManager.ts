import { DefaultManager } from "@aicacia/engine";
import { RigidBody2D } from "./RigidBody2D";

export class RigidBody2DManager extends DefaultManager<RigidBody2D> {}
